<!DOCTYPE html>
<html>
<head>
    <title>Reference as functions return values</title>
</head>
<body>
<?php 
        function ref_return() {
			global $a;
			$a = $a * 2;
			return $a;
		}
		$a = 10;
		$b = ref_return();  // изменилось $a и значение присвоено $b
	
		echo "a: {$a} / b: {$b}<br />"; // 20/20
		$b = 30;            // изменилось только $b
		echo "a: {$a} / b: {$b}<br />"; // 20/30
		
		
		function ref_return2() {
			global $a;
			$a = $a * 2;
			return $a;  // returns value (integer), ex., 20
			            // возвращает значение, а не переменную
		}
		$a = 10;
		$b =& ref_return2(); // ref_return2() возвращает 20, а не $a
		// Поэтому тут будет предупреждение:
		// Notice: Only variables should be assigned by reference
		// Выражение $b =& 20; бессмысленно, т.к. ссылаться на самостоятельное значение нет смысла,
		// достаточно использовать обычное присвоение $b = 20;
	
		// $a изменилось этим ref_return2() и стало 20
		// $b ссылается на 20
		echo "a: {$a} / b: {$b}<br />"; // 20/20
		$b = 30;
		echo "a: {$a} / b: {$b}<br /><br />"; // 20/30
		
		
		function &ref_return3() {
			global $a;
			$a = $a * 2;
			return $a;  // returns variable $a
			            // возвращает переменную
		}
		$a = 10;
		$b =& ref_return3(); // ref_return3() возвращает $a
	// только при обоих & возвращается $a, иначе возвращается 20,
	// и тут $b ссылается на возвращенную $a: $b =& $a;
	    // $a изменилось этим ref_return2() и стало 20
		// $b ссылается на $a
		echo "a: {$a} / b: {$b}<br />"; // 20/20
		$b = 30; // теперь $b перезаписывает и $a
		echo "a: {$a} / b: {$b}<br />"; // 30/30
		
		echo "<hr>";
		
		function &increment(){
		  static $var = 0;
		  $var++;
		  return $var;
		}
        $a =& increment(); // (из-за обоих &) возвращается не значение, а $var со значением  
		// означает $a =& $var; ссылается на $var 
		// теперь 1 у обоих		
        increment(); // отработала функция с инкрементом и
        // (благодаря поведению static) сохранила значение внутри себя,
		// теперь 2 внутри функции, и (из-за ссылки) 2 у $a
		$a++;        // теперь 3 у $a, и (из-за ссылки) 3 внутри функции
		increment(); // теперь 4 внутри функции, и (из-за ссылки) 4 у $a
		
		echo "a: {$a}<br />";   // 4 ($a incremented with $var)
		
		
		
?>
</body>
</html> 