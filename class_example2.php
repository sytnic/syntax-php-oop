<?php
class Person {
	function say_hello() {
		echo "Hello from inside a class.<br />";
	}
}

$methods = get_class_methods('Person');
foreach($methods as $method) {
	echo $method . "<br />";    // say_hello
}

if(method_exists('Person', 'say_hello')) {  // T/F
	echo "Method does exist.<br />";
} else {
	echo "Method does not exist.<br />";
}





?>