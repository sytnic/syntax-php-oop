<?php 
      // Способы шаблонирования:
      // 1. Set variables/include PHP file. - Here.
      // 2. Load file/search and replace.
      // Вызывается template_variable.php.
?>
<html>
	<head>
	  <title><?php echo $page_title; ?></title>
	</head>
	<body>
	
		<h1><?php echo $page_title; ?></h1>

		<p><?php echo $content; ?></p>
		
	</body>
</html>