<?php
    // Для работы скрипта 
    // 1 - скачан PHPMailer как zip архив
    // и помещен без изменений как папка на этом же уровне 
    // https://github.com/PHPMailer/PHPMailer
    // 2 - нужно указать реальные почты и пароль почты авторизации на сервере smtp

    // Include the PHPMailer classes
	// If these are located somewhere else, simply change the path.

    use PHPMailer\PHPMailer\PHPMailer;

    require_once("phpMailer/src/PHPMailer.php");
	require_once("phpMailer/src/SMTP.php");
	require_once("phpMailer/language/phpmailer.lang-ru.php");	

    // пример использования класса
    // необходимо и use (при использовании namespace у класса), и require_once
    //use MyMail\PHPMail;
    //require_once("phpMailer/src/phpmail.php");
    //$mail = new PHPMail();
    //exit();

	// mostly the same variables as before
	// ($to_name & $from_name are new, $headers was omitted) 
	$from_name = "Nj Box";
    /** 
     * Тут почта от кого уходит письмо
    */
	$from = "from@mail.ru";
    $to_name = "Junk mail";
    /** 
     * Тут почта к кому приходит письмо
    */
	$to = "to@mail.ru";
	$subject = "Mail Test at ".strftime("%T", time());

    /*
    $message = "<html>
                <head>
                  <style>
                    body {background-color: powderblue;}
                    h1   {color: blue;}
                    p    {color: red;}
                  </style>
                </head>
                <body>  
                  <h1>This is a heading</h1>
                  <p>This is a paragraph.</p> 
                </body>
                </html> ";
    */            
    /*
    $message = "<html>
                <body>
                  <h1>The span element</h1>
                    <p>My mother has <span style=\"color:blue;font-weight:bold\">blue</span> eyes 
                    and my father has <span style=\"color:darkolivegreen;font-weight:bold\">dark green</span> eyes.</p>
                </body>
                </html>";
    */
    $message = "This is a test.";
	$message = wordwrap($message,70);
	
	
	// PHPMailer's Object-oriented approach
	$mail = new PHPMailer();
	
	// Can use SMTP
	// comment out this section and it will use PHP mail() instead
	// SMTP используется вопреки настройкам сервера, но
    // блок может быть закомментирован и тогда будет использоваться
    // серверный (локальный) sendmail.exe,
    // локальные письма тут: openserver\userdata\temp\email
    $mail->isSMTP();
	$mail->Host     = "smtp.mail.ru";
	$mail->Port     = 25;
	$mail->SMTPAuth = true;  // будет аутентификация при помощи логина и пароля
    /** 
     * Тут должна быть почта логина
    */
	$mail->Username = "login@mail.ru";
	/** 
     * Тут должен быть пароль почты логина
    */
    $mail->Password = "password";
	

    // Переменные
	// Could assign strings directly to these, I only used the 
	// former variables to illustrate how similar the two approaches are.
	$mail->FromName = $from_name;
	$mail->From     = $from;
	$mail->addAddress($to, $to_name);
    
    $mail->isHTML(true);        // Set email format to HTML

	$mail->Subject  = $subject;
	$mail->Body     = $message;

    // Достаточно одной строчки для вложения
    //$mail->addAttachment('phpMailer/phpmailer_mini.png');
	
	$result = $mail->send();
	echo $result ? 'Sent' : 'Error';
  


?>