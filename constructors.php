<?php 

class Table {
	public $legs;
	static public $total_tables=0;
    
    function __construct() {
		$this->legs = 4;
		Table::$total_tables++;
	}	
	
}

$table = new Table();                // $total_tables already 1
echo $table->legs."<br>";            // $table->legs == 4

echo Table::$total_tables ."<br />"; // 1
$t1 = new Table();                   // $total_tables already 2
echo Table::$total_tables ."<br />"; // 2
$t2 = new Table();                   // $total_tables already 3
echo Table::$total_tables ."<br />"; // 3
                    // логика такая:
                    // __construct срабатывает при каждом вызове new Table()
echo $t2->legs."<br>";  // 4 legs

?>