<?php
// with static methods you can't use $this!
class Student {
  static $total_students=0;

	static public function add_student() {
	  Student::$total_students++;   //  self::$total_students++;
	}
	
  static function welcome_students($var="Hello") {
    echo "{$var} students.";
  }
}

$total_students = 20;  // отдельная переменная вне классов
// $student = new Student();
// echo $student->total_students;
echo Student::$total_students ."<br />";               // 0

echo $total_students."<br />";                         // 20

echo Student::welcome_students() ."<br />";            // string
echo Student::welcome_students("Greetings") ."<br />"; // string
Student::$total_students = 1;                          // overwrite
echo Student::$total_students ."<br />";               // 1


// static variables are shared throughout the inheritance tree.
// статичные переменные - общие для всех классов при наследовании
  class One {
    static $foo;
  }
  class Two extends One { }
  class Three extends One { }
  
  One::$foo = 1;
  Two::$foo = 2;
  //Three::$foo = 3;
  echo One::$foo;   // 2
  echo Two::$foo;   // 2
  echo Three::$foo; // 2

  // $make::error();  Fatal error


?>