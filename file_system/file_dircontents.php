<?php

// Like fopen/fread/fclose 
// opendir()
// readdir()
// closedir()

$dir = "."; // точка - текущая папка
if(is_dir($dir)) { // является ли она папкой
  if($dir_handle = opendir($dir)) { // если да, то открываем папку и получаем указатель, и, если это успешно, то
    while($filename = readdir($dir_handle)) { // пока что-то читаем из папки, возвращаем это
	// будут возвращены названия вложенных папок и файлов
      echo "filename: {$filename}<br />";
    }
	// use rewinddir($dir_handle) to start over
	// - используйте rewinddir($dir_handle) чтобы переместиться в начало списка в папке при необходимости. 
  // При закрытии closedir() этот указатель исчезает и его уже нельзя переместить.
	closedir($dir_handle);
  }
}  
echo "<hr />";

// еще один способ того же самого
// scandir(): reads all filenames into an array
// scandir(): считывает названия всех файлов в массив и
// не показывает вложенные папки
if(is_dir($dir)) {
  $dir_array = scandir($dir);
  foreach($dir_array as $file) {
    if(stripos($file, '.') > 0) { // убираем из списка все папки, начинающиеся с точки (.), (..)
      echo "filename: {$file}<br />";
    }
  }
}
// not much shorter, but maybe less complicated
// makes things like reverse order much easier



?>