<?php
      // Способы шаблонирования:
      // 1. Set variables/include PHP file. 
      // 2. Load file/search and replace. - Here.

      /**
       * @param string $filename
       * @param array  $assigned_vars
       * 
       * @return string
       */
	function display_template($filename, $assigned_vars) {
	  if(file_exists($filename)) {
        // получен файл как строка
	    $output = file_get_contents($filename);

	    foreach($assigned_vars as $key => $value) {
          // Выполняет поиск совпадений в строке $output с шаблоном {key} и заменяет их на $value.
	      $output = preg_replace('/{'.$key.'}/', $value, $output);
	    }
	    echo $output;
	  } else {
	    echo "*** Missing template error ***";
	  }
	}

	$template = "template2.php";
	$assigned_vars = array('page_title' => "Template Replace", 
	'content' => "This is a test of templating using search and replace.");

	display_template($template, $assigned_vars);

?>