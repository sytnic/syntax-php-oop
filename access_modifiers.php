<?php

class Example {
  public $a=1;
  protected $b=2;
  private $c=3;
  
  function show_abc() {
    echo $this->a;
    echo $this->b;
    echo $this->c;
  }
  
}

$example = new Example();
echo "public a: {$example->a}<br />";  // 1
//echo "protected b: {$example->b}<br />"; // Fatal error
//echo "private c: {$example->c}<br />"; // Fatal error

$example->show_abc(); // 123
echo "<br />";

class Example2 extends Example {
	
}
$example2 = new Example2();
$example2->show_abc(); // 123
echo "<br />";




?>