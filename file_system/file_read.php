<?php

$file = 'filetest.txt';
if($handle = fopen($file, 'r')) { // read
	$content = fread($handle, 6); // прочитает 6 байт 
                // 1 символ = 1 byte in eng
                // 1 символ = 2 байта in rus
	fclose($handle);
}

echo $content;
echo "<br />";
echo nl2br($content);
echo "<hr />";

// прочитать весь файл (не зная количество байт)
// use filesize() to read the whole file
$file = 'filetest.txt';
if($handle = fopen($file, 'r')) {  // read
  $content = fread($handle, filesize($file));
  fclose($handle);
}

echo nl2br($content);
echo "<hr />";

// получить весь контент файла
// file_get_contents(): shortcut for fopen/fread/fclose
// companion to shortcut file_put_contents()
$content = file_get_contents($file);
echo $content;
echo "<hr/>";

// получение контента из файла построчно
// incremental reading
$file = 'filetest.txt';
$content = "";
if($handle = fopen($file, 'r')) {  // read
	while(!feof($handle)) {         // пока не достигнут конец файла
		$content .= fgets($handle);  // получать контент построчно
	}
  fclose($handle);
}

echo $content;
echo "<hr/>";

?>