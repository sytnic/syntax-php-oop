<!DOCTYPE html>
<html>
<head>
    <title>Variable Scope</title>
</head>
<body>
    <?php
	
	$var = 1;
	function test1() {
		$var = 2;
		echo $var . "<br />";
	}
	test1();               // 2
	echo $var . "<br />";  // 1
		
	echo "<hr />";
	
	$var = 1;
	function test2() {
		global $var;
		$var = 2;          // перезаписал глобальную $var
		echo $var . "<br />";
	}
	test2();               // 2
	echo $var . "<br />";  // 2
	
	echo "<hr />";
	
	$var = 1;
	function test3() {
		$var = 2;
		echo $var . "<br />";
		$var++;
	}
	test3();               // 2
	test3();               // 2
	test3();               // 2
	echo $var . "<br />";  // 1
	
	echo "<hr />";
	
	$var = 1;
	function test4() {
		static $var = 2; // строка работает только при первом вызове, при следующих пропускается. 
		// поведение, как у глобальной переменной, но внутри функции, значение меняется при следующих вызовах.
		// значение сохраняется внутри функции и не влияет на глобальную видимость.
		echo $var . "<br />";
		$var++;
	}
	test4();               // 2
	test4();               // 3
	test4();               // 4
	echo $var . "<br />";  // 1
	
	
	
	
	?>


</body>
</html> 