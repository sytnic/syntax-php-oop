<?php

echo __FILE__ ."<br />";                                            // путь этого файла
echo __LINE__ ."<br />"; // be careful once you include files       // 4
echo dirname(__FILE__) ."<br >";                                    // путь текущей папки
echo __DIR__ ."<br />"; // only PHP 5.3                             // путь текущей папки

echo file_exists(__FILE__) ? 'yes' : 'no';                          // yes, этот файл существует
echo "<br />";
echo file_exists(dirname(__FILE__)."/basic.html") ? 'yes' : 'no';   // no, такой файл не существует
echo "<br />";
echo file_exists(dirname(__FILE__)) ? 'yes' : 'no';                 // yes, текущая папка существует
echo "<br />";

echo is_file(dirname(__FILE__)."/file_basics.php") ? 'yes' : 'no';  // yes, указанный файл является файлом
echo "<br />";
echo is_file(dirname(__FILE__)) ? 'yes' : 'no';                     // no, текущая папка не является файлом
echo "<br />";

echo is_dir(dirname(__FILE__)."/basic.html") ? 'yes' : 'no';        // no, этот файл не является папкой
echo "<br />";
echo is_dir(dirname(__FILE__)) ? 'yes' : 'no';                      // yes, текущая папка является папкой
echo "<br />";
echo is_dir('..') ? 'yes' : 'no';                                   // yes, надпапка является папкой
echo "<br />";

?>