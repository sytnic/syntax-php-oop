<?php

// используется установленная программа modules\sendmail\sendmail.exe ,
// она может быть установлена и в Windows, и в Linux ,
// прописывается в параметре sendmail_path в phpinfo() и php.ini

// отправляется во временную папку локального сервера или,
// если на локальном сервере настроена отправка через SMTP,
// на почту

// Openserver settings:
// https://help.mail.ru/mail/mailer/popsmtp
// "Отправлять почту через удалённый SMTP сервер"
// SMTP server - smtp.mail.ru
// port        - 25 (465 не сработал)
// user name   - полное имя почты (с какой, условно, пойдёт отправка)
// password    - пароль от указанной почты
// Шифрование  - Авто (также работает TLS; "Нет" и SSL - не сработали)
// Email отправителя - пустой (т.к. настраивается тут в заголовках, в $from)
// Остальное (POP3)  - пустое

/**
 *  Тут должна быть почта получателя
 */
$to = "to@mail.ru";      

    // Другие типы задания переменной $to :

    // Windows may not handle this format well
	// $to = "Recipient Name <nobody@somedomain.com>";

	// multiple recipients
	// $to = "nobody@somedomain.com, nobody@somedomain.com";
	// $to = "Recipient Name <nobody@somedomain.com>, nobody@somedomain.com";

$subject = "Mail Test at ".strftime("%T", time());

// теги (без заголовка "Content-Type: text/html") не работают,
// могут отправляться полноценные html-страницы со стилями в хедере, но некоторые стили могут не отрабатывать
$message = "<h3>This is a test</h3> Test text. <br> 
<p>This is a <strong>strong</strong>, <em>emphasize</em>, <i>italics</i> . </p>
<p> <span style=\"color:red;\"> This is some </span> <u>mispeled</u> text. </p>";  

// старые почтовые сервера обрезали каждую строку письма после 70 символа (или 72,75,78),
// поэтому можно самому назначить перенос строк с помощью wordwrap()
// Optional: Wrap lines for old email programs
// wrap at 70/72/75/78
$message = wordwrap($message,70);

/**
 *  Тут должна быть почта отправителя
 */
$from = "Recipient Name <from@mail.ru>";
    
    // типы задания переменной $from :

    // $from = "from@mail.ru";
    // $from = "From Name <from@mail.ru> - для отображения имени вместо почты

// Стандартные заголовки:
// для переносов строк (для заголовков) может понадобиться \n или \r\n
$headers = "From: {$from}\n";

// С этим заголовком письмо не доходит, теряется где-то в спам-фильтрах
// $headers = "Reply-To: {$from}\n";

// $headers .= "Cc: {$to}\n";   // копия
// $headers .= "Bcc: {$to}\n";  // слепая копия

// Нестандартные (собственные) заголовки должны начинаться с "X-":
$headers .= "X-Mailer: PHP/".phpversion()."\n";

$headers .= "MIME-Version: 1.0\n";

// text/plain, text/html
$headers .= "Content-Type: text/html; charset=iso-8859-1";

// Можно также указывать заголовки файлов Вложения

$result = mail($to, $subject, $message, $headers);

echo $result ? 'Sent' : 'Error';

?>