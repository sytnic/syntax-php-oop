<?php

class Box {

    public $name="box";

}

$box = new Box();             // новый объект

$box_reference = $box;        // новый объект, сцепленный с объектом $box (ссылка)

$box_clone = clone $box;      // новый объект, копия $box

$box_changed = clone $box;    // новый объект, копия $box

$box_changed->name = "changed box";  // изменения (только) в $box_changed

$another_box = new Box();     // новый объект

// == is casual and just checks to see if the attributes are the same
// сравниваются, равны ли атрибуты объектов
echo $box == $box_reference ? 'true' : 'false'; // true
echo "<br />";
echo $box == $box_clone ? 'true' : 'false';     // true
echo "<br />";
echo $box == $box_changed ? 'true' : 'false';   // false
echo "<br />";
echo $box == $another_box ? 'true' : 'false';   // true
echo "<br /><br />";    

// === is strict and checks to see if they reference the same object
// сранивается, один ли и тот же это объект
echo $box === $box_reference ? 'true' : 'false'; // true
echo "<br />";
echo $box === $box_clone ? 'true' : 'false';     // false
echo "<br />";
echo $box === $box_changed ? 'true' : 'false';   // false
echo "<br />";
echo $box === $another_box ? 'true' : 'false';   // false
echo "<br />";


?>