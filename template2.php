<?php 
      // Способы шаблонирования:
      // 1. Set variables/include PHP file.
      // 2. Load file/search and replace. - Here.
      // Вызывается template_replace.php или template_replace_oo.php.
?>
<html>
	<head>
	  <title>{page_title}</title>
	</head>
	<body>
	
		<h1>{page_title}</h1>

		<p>{content}</p>
		
	</body>
</html>
