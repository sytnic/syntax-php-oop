<?php
      // Способы шаблонирования:
      // 1. Set variables/include PHP file.
      // 2. Load file/search and replace. - Here.
      // Вызывается template_replace.php или template_replace_oo.php.

	class Template {
		
		public $filename;
	    public $assigned_vars=array();
		
		public function set($key, $value) {
	        $this->assigned_vars[$key] = $value;
	    }
	  
		public function display() {
		  if(file_exists($this->filename)) {
            // получен файл как строка
		    $output = file_get_contents($this->filename);

            // используем предварительно созданный массив в атрибуте $assigned_vars
		    foreach($this->assigned_vars as $key => $value) {
              // Выполняет поиск совпадений в строке $output с шаблоном {key} и заменяет их на $value.  
		      $output = preg_replace('/{'.$key.'}/', $value, $output);
		    }
		    echo $output;
		  } else {
		    echo "*** Missing template error ***";
		  }
		}
	}
	
	$template = new Template();
	$template->filename = "template2.php";

    // так создаётся массив в атрибуте $assigned_vars
	$template->set('page_title', "Template Replace OOP");
	$template->set('content', "This is a test of templating using search and replace.");
	//print_r ($template->assigned_vars);
    
    $template->display();

?>