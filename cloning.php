<?php

class Beverage {

  public $name;
  
  // __construct срабатывает только при создании нового объекта: new Beverage()
  // и не срабатывает при клонировании
  function __construct() {
    echo "New beverage was created.<br />";
  }

  // __clone срабатывает при клонировании
  function __clone() {
    echo "Existing beverage was cloned.<br />";
  }
  
 }
 
$a = new Beverage(); // "New beverage..."

$a->name = "coffee";

// the same objects
$b = $a; // always a reference with objects

$b->name = "tea";
echo $a->name;
echo "<br />";      // tea

// different objects
$c = clone $a;      // "Existing beverage..."

$c->name = "orange juice";
echo $a->name;      // tea
echo "<br />";
echo $c->name;      // orange juice
 


 
 
 
?>