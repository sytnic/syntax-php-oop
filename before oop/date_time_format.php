<!DOCTYPE html>
<html>
<head>
    <title>Date and Times: Formatting</title>
</head>
<body>
    <?php
	    $timestamp = time(); // now
		echo strftime("The date today is %H:%M %m/%d/%y", $timestamp);
		echo "<br />";

		$timestamp = 2000000000; 
		echo strftime("The date of 2 000 000 000 seconds is %H:%M %m/%d/%y", $timestamp);
		echo "<br />";

		$timestamp = 2000000000; 
		echo strftime("The date of 2 000 000 000 seconds is %c", $timestamp);
		echo "<br />";

		$timestamp = 1000000000; 
		echo strftime("The date of 1 000 000 000 seconds is %c", $timestamp);
		echo "<br />";

		$timestamp = 3000000000; 
		echo strftime("The date of 3 000 000 000 seconds is %c", $timestamp);
		echo "<br />";

		$timestamp = 4000000000; 
		echo strftime("The date of 4 000 000 000 seconds is %c", $timestamp);
		echo "<br />";

        function strip_zeros_from_date($marked_string="") {
			// remove the marked zeros
			// example, for *01
			$no_zeros = str_replace('*0', '', $marked_string);
			// then remove any remaining marks
			// example, for *12
			$cleaned_string = str_replace('*', '', $no_zeros);
			return $cleaned_string;
		}		
	  
        // good for example 02/22/2021 	  
	    echo strip_zeros_from_date(strftime("The date is *%m/*%d/%y", $timestamp));
		
		echo "<hr>";
		
		// for MySQL
		
		$dt = time();  // now
		
		// 2021-02-22 13:49:19
		$mysql_datetime = strftime("%Y-%m-%d %H:%M:%S", $dt);
		echo $mysql_datetime;
		echo "<br>";
		
		// 2021-02-22 
		$mysql_date = strftime("%Y-%m-%d", $dt);
		echo $mysql_date;
	  
	?>
</body>
</html> 