<?php

// we already used:
// 	dirname()
//	is_dir()

// getcwd(): Current Working Directory
echo getcwd() . "<br />";

// mkdir()
// может вызывать ошибки, если уже существует
mkdir('new', 0777); // 0777 is the PHP default

// на самом деле сработает mkdir('new', 0755), потому что
//    в каждой системе есть своя маска umask(), к-рая меняет указанные права на некоторую величину:
//    по умолчанию маска равна минус 0022.
// you can use umask() to change default permission settings
// default may be 0022

// recursive dir creation
// если указана больше, чем одна папка, нужно указать true
// может вызывать ошибки, если уже существует
mkdir('new/test/test2', 0777, true);

// изменить текущую рабочую директорию (Current Working Directory)
// changing dirs
chdir('new');
echo getcwd() . "<br />";

// удаляет директорию test2
// не удаляет, если в пути перечислена текущая рабочая директория
// removing a directory
rmdir('test/test2');

// есть нюансы у rmdir():
// must be closed and EMPTY before removal (and be CAREFUL)
// scripts to help you wipe out directories with files:
//  http://www.php.net/manual/en/function.rmdir.php


?>