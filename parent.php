<?php

class A {
  static $a=1;

  static function modified_a() {
    return self::$a + 10;
  }
  
  public function hello() {
		echo "Hello<br />";
	}   
}

class B extends A {
	static function attr_test() {
    echo parent::$a;  // сработает также A::$a и 
                      // (ссылка на самого себя) B::$a, т.к. использовалось наследование (extends)
  }
    static function method_test() {
    echo parent::modified_a();
  }  
    public function instance_test() {
		 echo parent::hello();       // it works
		// $this->hello();           // it works
	}
	
	public function hello() { 
		 echo "*******";
		 parent::hello();
		 echo "*******";
    }
	
}

echo B::$a . "<br />";             // 1
echo B::modified_a() . "<br />";   // 11

echo B::attr_test() ."<br />";     // 1
echo B::method_test() . "<br />";  // 11

$object = new B();
$object->instance_test();
$object->hello();

?>