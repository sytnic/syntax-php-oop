<?php

// отправляется во временную папку локального сервера или,
// если на локальном сервере настроена отправка через SMTP,
// на почту

// Openserver settings:
// https://help.mail.ru/mail/mailer/popsmtp
// "Отправлять почту через удалённый SMTP сервер"
// SMTP server - smtp.mail.ru
// port        - 25 (465 не сработал)
// user name   - полное имя почты (с какой, условно, пойдёт отправка)
// password    - пароль от указанной почты
// Шифрование  - Авто (также работает TLS; "Нет" и SSL - не сработали)
// Email отправителя - пустой (т.к. настраивается тут в заголовках, в $from)
// Остальное (POP3)  - пустое

/**
 *  Тут должна быть почта получателя
 */
$to = "to@mail.ru";      

    // Другие типы задания переменной $to :

    // Windows may not handle this format well
	// $to = "Recipient Name <nobody@somedomain.com>";

	// multiple recipients
	// $to = "nobody@somedomain.com, nobody@somedomain.com";
	// $to = "Recipient Name <nobody@somedomain.com>, nobody@somedomain.com";

$subject = "Mail Test at ".strftime("%T", time());

// старые почтовые сервера обрезали каждую строку письма после 70 символа или 72,75,78
// поэтому можно самому назначить перенос строк с помощью wordwrap()
$message = "<h3>This is a test<h3> Test. <br> This is a test.";  // теги (по умолчанию) не работают
// Optional: Wrap lines for old email programs
// wrap at 70/72/75/78
$message = wordwrap($message,70);

/**
 *  Тут должна быть почта отправителя
 */
$from = "Recipient Name <from@mail.ru>";
// типы задания переменной $from :
// $from = "from@mail.ru";
// $from = "From Name <from@mail.ru> - для отображения имени вместо почты

$headers = "From: {$from}";

$result = mail($to, $subject, $message, $headers);
echo $result ? 'Sent' : 'Error';

?>