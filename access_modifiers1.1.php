<?php

class Example {
  public $a=1;
  protected $b=2;
  private $c=3;
  
  function show_abc() {
    echo $this->a;
    echo $this->b;
    echo $this->c;
  }
  
  public function hello_everyone() {
    return "Hello everyone.<br />";
  }
  protected function hello_family() {
    return "Hello family.<br />";
  }
  private function hello_me() {
    return "Hello me.<br />";
  }
  
  //function is public by default
  function hello() {
    $output = $this->hello_everyone();
    $output .= $this->hello_family();
    $output .= $this->hello_me();
    return $output;
  }  
  
}

$example = new Example();
echo "public a: {$example->a}<br />";  // 1
//echo "protected b: {$example->b}<br />"; // Fatal error
//echo "private c: {$example->c}<br />"; // Fatal error

$example->show_abc(); // 123
echo "<br />";

class Example2 extends Example {
	
}
$example2 = new Example2();
$example2->show_abc(); // 123
echo "<br />";

echo "hello_everyone: {$example->hello_everyone()}<br />"; //  Hello everyone.
// echo "hello_family: {$example->hello_family()}<br />"; // Fatal error
//echo "hello_me: {$example->hello_me()}<br />"; // Fatal error

echo $example->hello(); // it works

?>