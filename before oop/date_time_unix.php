<!DOCTYPE html>
<html>
<head>
    <title>Date and Times: Unix</title>
</head>
<body>
    <?php
	    echo time();  // now
		echo "<br>";
		// hour, min, sec, month, day, year
		echo mktime(0,0,01,6,1,2019);
		// returns seconds or false
		echo "<br>";
		
		//checkdate
		// month, day, year
		echo checkdate(12,31,2000) ? 'true' : 'false';
        echo "<br>";	
        echo checkdate(2,31,2000) ? 'true' : 'false';
        echo "<br>";	

        //$unix_timestamp = strtotime("now");	
        //$unix_timestamp = strtotime("1 June 2019"); 		
	    //$unix_timestamp = strtotime("June 1, 2019");	
		//$unix_timestamp = strtotime("+1 day");	
		//$unix_timestamp = strtotime("last Monday");	
		$unix_timestamp = strtotime("2019-06-01");	
		
		echo $unix_timestamp."<br>";
	?>
</body>
</html> 