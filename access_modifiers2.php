<?php
// Setters and Getters

class SetterGetterExample {
  private $a=1;

  private $b=2;
  
  public function get_a() {
    return $this->a;
  }

  public function set_a($value) {
    $this->a = $value;
  }

  public function gb() {
    return $this->b;
  }

  public function sb($value) {
    $this->b = $value;
  }

}
$example = new SetterGetterExample();
// restricted: echo $example->a ."<br />";
echo $example->get_a() ."<br />"; // 1, получает значение атрибута (get)
$example->set_a(15);              // overwrite $a, задаёт значение атрибута (set)
echo $example->get_a() ."<br />"; // 15  

echo $example->gb() ."<br />"; // 2, получает значение атрибута (get)
$example->sb(10);              // overwrite $b, задаёт значение атрибута (set)
echo $example->gb() ."<br />"; // 10


?>