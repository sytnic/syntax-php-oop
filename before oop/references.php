<!DOCTYPE html>
<html>
<head>
    <title>Reference Assignment</title>
</head>
<body>
<?php 
    $a = 1;
	$b = $a;
	$b = 2;
	echo "a:{$a} / b: {$b}<br />";
	// returns 1/2
	
	$a = 1;
	$b =& $a;  // a kind of "automobile" clutch
	$b = 2;    // перезаписав $b, автоматом перезаписал $a
	echo "a:{$a} / b: {$b}<br />";
	// returns 2/2
	
	unset($b);  // стирание $b не стирает $a
	echo "a:{$a} / b: {$b}<br />";


?>
</body>
</html> 