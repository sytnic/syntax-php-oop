<?php

$file = 'filetest.txt';
if($handle = fopen($file, 'w')) {  // overwrite
  fwrite($handle, "123\n456\n789");

	$pos = ftell($handle);  // позиция указателя

	fseek($handle, $pos-6); // позиция перемещена к началу на 6 символов
	fwrite($handle, "abcdef"); // новые символы записаны в файл (перезаписывают существующие)

	rewind($handle);		// позиция курсора перемещена в начало файла
	fwrite($handle, "xyz"); // новые символы записаны в файл (перезаписывают существующие)
	
  fclose($handle);
}

// Beware, it will OVERTYPE!!!
// NOTE: a and a+ modes will not let you move the pointer

?>