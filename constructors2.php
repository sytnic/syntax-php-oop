<?php

class Table {
	public $legs;
	static public $total_tables=0;
	
  function __construct($leg_count=4) {
		$this->legs = $leg_count;
		Table::$total_tables++;
  }
  
  function __destruct() {
    Table::$total_tables--;
  }

}

$table = new Table();    // $leg_count == 4, how  new Table(4)
                         // $total_tables == 1
echo $table->legs ."<br />";         // 4

echo Table::$total_tables ."<br />"; // 1

$t1 = new Table(5);     // how __construct($leg_count=5)
echo "leg_count: ".$t1->legs ."<br />";     // $leg_count == 5

echo Table::$total_tables ."<br />"; 
// 2, срабатывает $total_tables++ в construct при каждом вызове new Table() 

$t2 = new Table(6);
echo "leg_count: ". $t2->legs ."<br />";    // $leg_count == 6, т.к. указано Table(6) для __construct($leg_count=4)

echo Table::$total_tables ."<br />";
// 3


?>