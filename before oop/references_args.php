<!DOCTYPE html>
<html>
<head>
    <title>Reference as functions arguments</title>
</head>
<body>
<?php 
  		function ref_test($var) {
			$var = $var * 2;
		}
		$a = 10;
		ref_test($a);
		echo $a;  // 10, потому что функция отработала и... ничего не сделала,
		          // ни echo, ни return, мы ни ловили в переменную
		echo "<br>";
		
		function ref_test2(&$var) {
			$var = $var * 2;
		}
		$a = 10;
		ref_test2($a);  // a kind of $a=&$var
		                // из-за ссылки (сцепления) функция отработала и изменила значение $a
		echo $a;    // 20
		echo "<br>";

   		// аналогично
   		function ref_test3($a) {
		    global $a;
			$a = $a * 2;
		}
		$a = 10;
		ref_test3($a);
		echo $a;	// 20  
        echo "<br>";	
    
		// или так (т.к. при global $a использование ($a) избыточно)
		function ref_test4() {
		    global $a;
			$a = $a * 2;
		}
		$a = 10;
		ref_test4();
		echo $a;	// 20  
        echo "<br>";			

?>
</body>
</html> 